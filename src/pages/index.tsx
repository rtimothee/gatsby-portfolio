import React from "react"
import "../styles/style.scss"

const IndexPage: React.FC = () => {
    return (
        <div className='container'>
            <div className="background">
                <div className="filter"></div>
            </div>
            <div className="content">
                <h1>Hi!<br/>
                    I'm <span className="name">Timothée Roldao</span>,
                </h1>
                <h2>FullStack Web Developer PHP at <a href='https://www.infomaniak.com' target='_blank'>Infomaniak</a></h2>

                <div className="links">
                    <a href="/CV_TimotheeRoldao.pdf">CV</a>
                    <a href="https://www.linkedin.com/in/rtimothee" target="_blank">LinkedIn</a>
                </div>
            </div>

            <div className="footer">
                <a href="https://www.infomaniak.com" target="_blank" rel="noopener noreferrer">Hébergé par Infomaniak</a>
            </div>

        </div>
    )
}

export default IndexPage
